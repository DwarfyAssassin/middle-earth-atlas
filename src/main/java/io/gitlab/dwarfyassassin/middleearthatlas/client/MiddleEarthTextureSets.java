package io.gitlab.dwarfyassassin.middleearthatlas.client;

import hunternif.mc.atlas.api.AtlasAPI;
import hunternif.mc.atlas.api.TileAPI;
import hunternif.mc.atlas.client.TextureSet;
import hunternif.mc.atlas.client.TextureSetMap;
import io.gitlab.dwarfyassassin.middleearthatlas.common.MiddleEarthAtlas;
import io.gitlab.dwarfyassassin.middleearthatlas.common.MiddleEarthAtlasConfig;
import net.minecraft.util.ResourceLocation;

public class MiddleEarthTextureSets {
    private static final TileAPI API = AtlasAPI.getTileAPI();
    public static TextureSet
    TEST_MEA,
    BAOBAB,
    BOULDERS,
    BUSHLAND,
    BUSHLAND_HILLS,
    CLOUD_FOREST,
    DEAD_FOREST,
    DUNES,
    FOREST_ASPEN,
    FOREST_BEECH,
    FOREST_KANUKA,
    FOREST_LARCH,
    FOREST_MANGROVE,
    FOREST_MAPLE,
    FOREST_PINE,
    GREENWOOD,
    JUNGLE_DENSE,
    LOTHLORIEN,
    MIRKWOOD,
    MORDOR_DESERT,
    MORGUL_PLAINS,
    ORCHARD_ALMOND,
    ORCHARD_APPLE_PEAR,
    ORCHARD_DATE,
    ORCHARD_LEMON,
    ORCHARD_LIME,
    ORCHARD_OLIVE,
    ORCHARD_ORANGE,
    ORCHARD_PLUM,
    ORCHARD_POMEGRANATE,
    PLAINS_CLEARING,
    SAVANNA_ARID,
    SAVANNA_ARID_HILLS,
    SCRUBLAND,
    SHRUBLAND,
    STEPPE,
    STEPPE_BARREN,
    VINEYARD_DORWINION,
    WASTELAND;
    
    
    //TODO add shore texture set support
    public static void createAndRegisterMiddleEarthBiomeTextureSets() {
        TEST_MEA = registerIfNull("TEST_MEA", MiddleEarthTextures.TILE_TEST_MEA);
        BAOBAB = registerIfNull("BAOBAB", MiddleEarthTextures.TILE_TEST_MEA);
        BOULDERS = registerIfNull("BOULDERS", MiddleEarthTextures.TILE_TEST_MEA);
        BUSHLAND = registerIfNull("BUSHLAND", MiddleEarthTextures.TILE_TEST_MEA);
        BUSHLAND_HILLS = registerIfNull("BUSHLAND_HILLS", MiddleEarthTextures.TILE_TEST_MEA);
        CLOUD_FOREST = registerIfNull("CLOUD_FOREST", MiddleEarthTextures.TILE_TEST_MEA);
        DEAD_FOREST = registerIfNull("DEAD_FOREST", MiddleEarthTextures.TILE_TEST_MEA);
        DUNES = registerIfNull("DUNES", MiddleEarthTextures.TILE_TEST_MEA);
        FOREST_ASPEN = registerIfNull("FOREST_ASPEN", MiddleEarthTextures.TILE_TEST_MEA);
        FOREST_BEECH = registerIfNull("FOREST_BEECH", MiddleEarthTextures.TILE_TEST_MEA);
        FOREST_KANUKA = registerIfNull("FOREST_KANUKA", MiddleEarthTextures.TILE_TEST_MEA);
        FOREST_LARCH = registerIfNull("FOREST_LARCH", MiddleEarthTextures.TILE_TEST_MEA);
        FOREST_MANGROVE = registerIfNull("FOREST_MANGROVE", MiddleEarthTextures.TILE_TEST_MEA);
        FOREST_MAPLE = registerIfNull("FOREST_MAPLE", MiddleEarthTextures.TILE_TEST_MEA);
        FOREST_PINE = registerIfNull("FOREST_PINE", MiddleEarthTextures.TILE_TEST_MEA);
        GREENWOOD = registerIfNull("GREENWOOD", MiddleEarthTextures.TILE_TEST_MEA);
        JUNGLE_DENSE = registerIfNull("JUNGLE_DENSE", MiddleEarthTextures.TILE_TEST_MEA);
        LOTHLORIEN = registerIfNull("LOTHLORIEN", MiddleEarthTextures.TILE_TEST_MEA);
        MIRKWOOD = registerIfNull("MIRKWOOD", MiddleEarthTextures.TILE_MIRKWOOD_TEST); // TODO rename
        MORDOR_DESERT = registerIfNull("MORDOR_DESERT", MiddleEarthTextures.TILE_TEST_MEA);
        MORGUL_PLAINS = registerIfNull("MORGUL_PLAINS", MiddleEarthTextures.TILE_TEST_MEA);
        ORCHARD_ALMOND = registerIfNull("ORCHARD_ALMOND", MiddleEarthTextures.TILE_TEST_MEA);
        ORCHARD_APPLE_PEAR = registerIfNull("ORCHARD_APPLE_PEAR", MiddleEarthTextures.TILE_TEST_MEA);
        ORCHARD_DATE = registerIfNull("ORCHARD_DATE", MiddleEarthTextures.TILE_TEST_MEA);
        ORCHARD_LEMON = registerIfNull("ORCHARD_LEMON", MiddleEarthTextures.TILE_TEST_MEA);
        ORCHARD_LIME = registerIfNull("ORCHARD_LIME", MiddleEarthTextures.TILE_TEST_MEA);
        ORCHARD_OLIVE = registerIfNull("ORCHARD_OLIVE", MiddleEarthTextures.TILE_TEST_MEA);
        ORCHARD_ORANGE = registerIfNull("ORCHARD_ORANGE", MiddleEarthTextures.TILE_TEST_MEA);
        ORCHARD_PLUM = registerIfNull("ORCHARD_PLUM", MiddleEarthTextures.TILE_TEST_MEA);
        ORCHARD_POMEGRANATE = registerIfNull("ORCHARD_POMEGRANATE", MiddleEarthTextures.TILE_TEST_MEA);
        PLAINS_CLEARING = registerIfNull("PLAINS_CLEARING", MiddleEarthTextures.TILE_TEST_MEA);
        SAVANNA_ARID = registerIfNull("SAVANNA_ARID", MiddleEarthTextures.TILE_TEST_MEA);
        SAVANNA_ARID_HILLS = registerIfNull("SAVANNA_ARID_HILLS", MiddleEarthTextures.TILE_TEST_MEA);
        SCRUBLAND = registerIfNull("SCRUBLAND", MiddleEarthTextures.TILE_TEST_MEA);
        SHRUBLAND = registerIfNull("SHRUBLAND", MiddleEarthTextures.TILE_TEST_MEA);
        STEPPE = registerIfNull("STEPPE", MiddleEarthTextures.TILE_TEST_MEA);
        STEPPE_BARREN = registerIfNull("STEPPE_BARREN", MiddleEarthTextures.TILE_TEST_MEA);
        VINEYARD_DORWINION = registerIfNull("VINEYARD_DORWINION", MiddleEarthTextures.TILE_TEST_MEA);
        WASTELAND = registerIfNull("WASTELAND", MiddleEarthTextures.TILE_TEST_MEA);
        
        if(MiddleEarthAtlasConfig.newTextures) {
            TextureSetMap.instance().markDirty();
        }

        MiddleEarthAtlas.logger.debug("Created and registered middle earth biome texture sets.");
    }
    
    public static void createAndRegisterMiddleEarthCustomTextureSets() {
        MiddleEarthAtlas.logger.debug("Created and registered middle earth custom texture sets.");
        
        
    }
    
    private static TextureSet registerIfNull(String name, ResourceLocation... textures) {
        if(MiddleEarthAtlasConfig.newTextures || !TextureSetMap.instance().isRegistered(name)) {
            return API.registerTextureSet(name, textures);
        }
        
        return TextureSetMap.instance().getByName(name);
    }
}
