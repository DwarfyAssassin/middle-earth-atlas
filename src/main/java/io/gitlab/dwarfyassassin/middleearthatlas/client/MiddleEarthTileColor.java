package io.gitlab.dwarfyassassin.middleearthatlas.client;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import hunternif.mc.atlas.api.AtlasAPI;
import hunternif.mc.atlas.client.TextureSet;
import io.gitlab.dwarfyassassin.middleearthatlas.common.MiddleEarthAtlas;
import io.gitlab.dwarfyassassin.middleearthatlas.common.MiddleEarthAtlasConfig;
import io.gitlab.dwarfyassassin.middleearthatlas.common.reflection.MiddleEarthAtlasReflectionHelper;
import lotr.common.fac.LOTRFaction;
import lotr.common.world.biome.LOTRBiome;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.util.ResourceLocation;

public class MiddleEarthTileColor {

    private static Map<LOTRBiome, Color> biomeColors;
    
    public static void registerColoredBiomes() {
        biomeColors = new HashMap<LOTRBiome, Color>();
        
        biomeColors.put(LOTRBiome.shire, new Color(LOTRFaction.HOBBIT.getFactionColor()));
        biomeColors.put(LOTRBiome.whiteDowns, new Color(LOTRFaction.HOBBIT.getFactionColor()));
        biomeColors.put(LOTRBiome.shireMarshes, new Color(LOTRFaction.HOBBIT.getFactionColor()));
        biomeColors.put(LOTRBiome.shireWoodlands, new Color(LOTRFaction.HOBBIT.getFactionColor()));

        biomeColors.put(LOTRBiome.breeland, new Color(LOTRFaction.BREE.getFactionColor()));
        biomeColors.put(LOTRBiome.chetwood, new Color(LOTRFaction.BREE.getFactionColor()));

        biomeColors.put(LOTRBiome.angle, new Color(LOTRFaction.RANGER_NORTH.getFactionColor()));

        biomeColors.put(LOTRBiome.blueMountains, new Color(LOTRFaction.BLUE_MOUNTAINS.getFactionColor()));
        biomeColors.put(LOTRBiome.blueMountainsFoothills, new Color(LOTRFaction.BLUE_MOUNTAINS.getFactionColor()));

        biomeColors.put(LOTRBiome.lindon, new Color(LOTRFaction.HIGH_ELF.getFactionColor()));
        biomeColors.put(LOTRBiome.lindonCoast, new Color(LOTRFaction.HIGH_ELF.getFactionColor()));
        biomeColors.put(LOTRBiome.lindonWoodlands, new Color(LOTRFaction.HIGH_ELF.getFactionColor()));
        biomeColors.put(LOTRBiome.rivendell, new Color(LOTRFaction.HIGH_ELF.getFactionColor()));
        biomeColors.put(LOTRBiome.rivendellHills, new Color(LOTRFaction.HIGH_ELF.getFactionColor()));

        biomeColors.put(LOTRBiome.ettenmoors, new Color(LOTRFaction.GUNDABAD.getFactionColor()));
        biomeColors.put(LOTRBiome.coldfells, new Color(LOTRFaction.GUNDABAD.getFactionColor()));

        biomeColors.put(LOTRBiome.angmar, new Color(LOTRFaction.ANGMAR.getFactionColor()));
        biomeColors.put(LOTRBiome.angmarMountains, new Color(LOTRFaction.ANGMAR.getFactionColor()));

        biomeColors.put(LOTRBiome.woodlandRealm, new Color(LOTRFaction.WOOD_ELF.getFactionColor()));
        biomeColors.put(LOTRBiome.woodlandRealmHills, new Color(LOTRFaction.WOOD_ELF.getFactionColor()));

        biomeColors.put(LOTRBiome.dolGuldur, new Color(LOTRFaction.DOL_GULDUR.getFactionColor()));
        
        biomeColors.put(LOTRBiome.dale, new Color(LOTRFaction.DALE.getFactionColor()));

        biomeColors.put(LOTRBiome.ironHills, new Color(LOTRFaction.DURINS_FOLK.getFactionColor()));
        biomeColors.put(LOTRBiome.erebor, new Color(LOTRFaction.DURINS_FOLK.getFactionColor()));

        biomeColors.put(LOTRBiome.lothlorien, new Color(LOTRFaction.LOTHLORIEN.getFactionColor()));
        biomeColors.put(LOTRBiome.lothlorienEdge, new Color(LOTRFaction.LOTHLORIEN.getFactionColor()));

        biomeColors.put(LOTRBiome.dunland, new Color(LOTRFaction.DUNLAND.getFactionColor()));

        biomeColors.put(LOTRBiome.nanCurunir, new Color(LOTRFaction.ISENGARD.getFactionColor()));
        biomeColors.put(LOTRBiome.rohanUrukHighlands, new Color(LOTRFaction.ISENGARD.getFactionColor()));

        biomeColors.put(LOTRBiome.fangorn, new Color(LOTRFaction.FANGORN.getFactionColor()));
        biomeColors.put(LOTRBiome.fangornClearing, new Color(LOTRFaction.FANGORN.getFactionColor()));
        
        biomeColors.put(LOTRBiome.rohan, new Color(LOTRFaction.ROHAN.getFactionColor()));
        biomeColors.put(LOTRBiome.rohanWoodlands, new Color(LOTRFaction.ROHAN.getFactionColor()));

        biomeColors.put(LOTRBiome.gondor, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.gondorWoodlands, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.entwashMouth, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.pelennor, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.ithilien, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.ithilienHills, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.lossarnach, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.imlothMelui, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.anduinMouth, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.pelargir, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.dorEnErnil, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.dorEnErnilHills, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.lamedon, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.lamedonHills, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.blackrootVale, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.pinnathGelin, new Color(LOTRFaction.GONDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.wold, new Color(LOTRFaction.GONDOR.getFactionColor()));

        biomeColors.put(LOTRBiome.mordor, new Color(LOTRFaction.MORDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.easternDesolation, new Color(LOTRFaction.MORDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.mordorMountains, new Color(LOTRFaction.MORDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.nurn, new Color(LOTRFaction.MORDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.nurnMarshes, new Color(LOTRFaction.MORDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.nurnen, new Color(LOTRFaction.MORDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.nanUngol, new Color(LOTRFaction.MORDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.gorgoroth, new Color(LOTRFaction.MORDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.morgulVale, new Color(LOTRFaction.MORDOR.getFactionColor()));
        biomeColors.put(LOTRBiome.udun, new Color(LOTRFaction.MORDOR.getFactionColor()));

        biomeColors.put(LOTRBiome.dorwinion, new Color(LOTRFaction.DORWINION.getFactionColor()));
        biomeColors.put(LOTRBiome.dorwinionHills, new Color(LOTRFaction.DORWINION.getFactionColor()));
        
        biomeColors.put(LOTRBiome.rhunIsland, new Color(LOTRFaction.RHUDEL.getFactionColor()));
        biomeColors.put(LOTRBiome.rhunIslandForest, new Color(LOTRFaction.RHUDEL.getFactionColor()));
        biomeColors.put(LOTRBiome.rhunLand, new Color(LOTRFaction.RHUDEL.getFactionColor()));
        biomeColors.put(LOTRBiome.rhunLandHills, new Color(LOTRFaction.RHUDEL.getFactionColor()));
        biomeColors.put(LOTRBiome.rhunLandSteppe, new Color(LOTRFaction.RHUDEL.getFactionColor()));
        biomeColors.put(LOTRBiome.rhunRedForest, new Color(LOTRFaction.RHUDEL.getFactionColor()));
        
        biomeColors.put(LOTRBiome.gulfHarad, new Color(LOTRFaction.NEAR_HARAD.getFactionColor()));
        biomeColors.put(LOTRBiome.gulfHaradForest, new Color(LOTRFaction.NEAR_HARAD.getFactionColor()));
        biomeColors.put(LOTRBiome.farHaradCoast, new Color(LOTRFaction.NEAR_HARAD.getFactionColor()));
        biomeColors.put(LOTRBiome.umbar, new Color(LOTRFaction.NEAR_HARAD.getFactionColor()));
        biomeColors.put(LOTRBiome.umbarForest, new Color(LOTRFaction.NEAR_HARAD.getFactionColor()));
        biomeColors.put(LOTRBiome.umbarHills, new Color(LOTRFaction.NEAR_HARAD.getFactionColor()));
        biomeColors.put(LOTRBiome.nearHaradFertile, new Color(LOTRFaction.NEAR_HARAD.getFactionColor()));
        biomeColors.put(LOTRBiome.nearHaradFertileForest, new Color(LOTRFaction.NEAR_HARAD.getFactionColor()));
        biomeColors.put(LOTRBiome.harnedor, new Color(LOTRFaction.NEAR_HARAD.getFactionColor()));
        
        // Morwaith & Taurethirm don't have any specific biomes

        biomeColors.put(LOTRBiome.pertorogwaith, new Color(LOTRFaction.HALF_TROLL.getFactionColor()));
    }
    
    public static boolean hasBiomeFactionColor(String tileName) {
        return MiddleEarthAtlasConfig.factionColoredBiomeTiles && getBiomeFactionColor(tileName) != null;
    }
    
    public static Color getBiomeFactionColor(String tileName) {
        int index = tileName.indexOf("_");
        String biomeNanme = index == -1 ? tileName : tileName.substring(0, index);
        LOTRBiome biome = MiddleEarthAtlasReflectionHelper.getLOTRBiome(biomeNanme);
        return biomeColors.get(biome);
    }
    
    public static TextureSet getAndRegisterBiomeColoredTextureSet(String tileName, TextureSet baseTextureSet) {
        if(!MiddleEarthAtlasConfig.factionColoredBiomeTiles || baseTextureSet == MiddleEarthTextureSets.TEST_MEA) return baseTextureSet;
        
        Color color = getBiomeFactionColor(tileName);
        if(color != null) {
            return getAndRegisterColoredTextureSet(color, baseTextureSet);
        }
        
        return baseTextureSet;
    }
    
    private static TextureSet getAndRegisterColoredTextureSet(Color factionColor, TextureSet baseTextureSet) {
        Minecraft mc = Minecraft.getMinecraft();
        Color color = getMixedColor(factionColor);
        String newTextureName = baseTextureSet.name + "_" + Integer.toHexString(factionColor.getRGB());
        
        ResourceLocation[] oldTextures = baseTextureSet.textures;
        ResourceLocation[] newTextures = new ResourceLocation[oldTextures.length];
        
        for(int i = 0; i < oldTextures.length; i++) {
            BufferedImage oldImg = null;
            try {
                InputStream stream = mc.getResourceManager().getResource(oldTextures[i]).getInputStream();
                oldImg = ImageIO.read(stream);
                stream.close();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
            
            if(oldImg == null) {
                MiddleEarthAtlas.logger.error("Couldn't read texture " + oldTextures[i] + " to make colored texture set " +  newTextureName);
                return baseTextureSet;
            }
            
            
            int imgWidth = oldImg.getWidth();
            int imgHeight = oldImg.getHeight();
            int[] colors = oldImg.getRGB(0, 0, imgWidth, imgHeight, null, 0, imgWidth);
            for (int i2 = 0; i2 < colors.length; i2++) {
                colors[i2] = new Color(color.getRed(), color.getGreen(), color.getBlue(), (colors[i2] >> 24) & 0xff).getRGB();
            }
            BufferedImage newImg = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_ARGB);
            newImg.setRGB(0, 0, imgWidth, imgHeight, colors, 0, imgWidth);
            
            
            newTextures[i] = mc.getTextureManager().getDynamicTextureLocation(MiddleEarthAtlas.MODID + "." + newTextureName, new DynamicTexture(newImg));
        }
        
        return AtlasAPI.getTileAPI().registerTextureSet(newTextureName, newTextures);
    }
    
    private static Color getMixedColor(Color factionColor) { 
        float[] hsv = Color.RGBtoHSB(factionColor.getRed(), factionColor.getGreen(), factionColor.getBlue(), null);
        int newRGB = Color.HSBtoRGB(hsv[0], hsv[1], hsv[2] < 0.2f ? hsv[2] : 0.2f);
        return new Color(newRGB);
    }
}
