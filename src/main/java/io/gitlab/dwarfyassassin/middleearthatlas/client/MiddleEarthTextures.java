package io.gitlab.dwarfyassassin.middleearthatlas.client;

import io.gitlab.dwarfyassassin.middleearthatlas.common.MiddleEarthAtlas;
import net.minecraft.util.ResourceLocation;

public class MiddleEarthTextures {
    public static final String GUI_TILES = "textures/gui/tiles/";
    
    public static final ResourceLocation
    TILE_TEST_MEA = tile("test_MEA.png"),
    TILE_MIRKWOOD_TEST = tile("mirkwood_test.png"); // TODO rename
    
    
    private static final ResourceLocation tile(String fileName) {
        return new ResourceLocation(MiddleEarthAtlas.MODID, GUI_TILES + fileName);
    }
}
