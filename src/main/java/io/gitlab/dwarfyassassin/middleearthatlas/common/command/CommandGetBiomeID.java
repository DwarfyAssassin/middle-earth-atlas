package io.gitlab.dwarfyassassin.middleearthatlas.common.command;

import hunternif.mc.atlas.AntiqueAtlasMod;
import hunternif.mc.atlas.core.AtlasData;
import hunternif.mc.atlas.core.DimensionData;
import hunternif.mc.atlas.ext.ExtBiomeData;
import hunternif.mc.atlas.ext.ExtTileIdMap;
import lotr.common.LOTRDimension;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;

public class CommandGetBiomeID extends CommandBase {

    @Override
    public String getCommandName() {
        return "getBiomeID";
    }


    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        EntityPlayerMP player = getCommandSenderAsPlayer(sender);
        World world = player.worldObj;
        int posX = (int) Math.floor(player.posX);
        int posZ = (int) Math.floor(player.posZ);
        int chunkX = posX >> 4;
        int chunkZ = posZ >> 4;
        player.addChatMessage(new ChatComponentText("pos: " + posX + ", " + posZ + " --- chunk pos: " + chunkX + ", " + chunkZ));

        ExtBiomeData data = AntiqueAtlasMod.extBiomeData.getData();
        int biomeID = data.getBiomeIdAt(world.provider.dimensionId, chunkX, chunkZ);
        String biomeName = ExtTileIdMap.instance().getPseudoBiomeName(biomeID);
        player.addChatMessage(new ChatComponentText("global/override biome ID: " + biomeID + " --- name: " + biomeName));
        
        AtlasData atlasData = AntiqueAtlasMod.atlasData.getAtlasData(0, player.worldObj);
        DimensionData dimData = atlasData.getDimensionData(player.worldObj.provider.dimensionId);
        biomeID = dimData.getTile(chunkX, chunkZ).biomeID;
        biomeName = BiomeGenBase.getBiome(biomeID).biomeName;
        if(player.dimension == LOTRDimension.MIDDLE_EARTH.dimensionID) biomeName = ExtTileIdMap.instance().getPseudoBiomeName(biomeID);
        player.addChatMessage(new ChatComponentText("atlas0 biome ID: " + biomeID + " --- name: " + biomeName));
    }


    @Override
    public String getCommandUsage(ICommandSender sender) {
        return null;
    }

  
}
