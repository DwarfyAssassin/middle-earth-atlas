package io.gitlab.dwarfyassassin.middleearthatlas.common;

import hunternif.mc.atlas.core.BiomeDetectorBase;
import hunternif.mc.atlas.core.IBiomeDetector;
import hunternif.mc.atlas.ext.ExtTileIdMap;
import io.gitlab.dwarfyassassin.middleearthatlas.common.reflection.MiddleEarthAtlasReflectionHelper;
import lotr.common.LOTRDimension;
import lotr.common.world.LOTRWorldChunkManager;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.BiomeDictionary.Type;

public class BiomeDetectorMiddleEarth extends BiomeDetectorBase implements IBiomeDetector {
    private boolean doScanPonds = true;
    private static final boolean[] waterOverrideBiomes = new boolean[LOTRDimension.MIDDLE_EARTH.biomeList.length];
    private static final boolean[] waterOverrideVaraints = new boolean[MiddleEarthAtlasReflectionHelper.getBiomeVariantList().length];
    

    public static void scanBiomeTypes() {
        for(LOTRBiome biome : LOTRBiomeDictionary.getBiomesForType(Type.BEACH)) {
            waterOverrideBiomes[biome.biomeID] = true;
        }
        for(LOTRBiome biome : LOTRBiomeDictionary.getBiomesForType(Type.SWAMP)) {
            waterOverrideBiomes[biome.biomeID] = true;
        }
        
        for(LOTRBiomeVariant variant : LOTRBiomeDictionary.getVariantsForType(Type.BEACH)) {
            waterOverrideVaraints[variant.variantID] = true;
        }
        for(LOTRBiomeVariant variant : LOTRBiomeDictionary.getVariantsForType(Type.SWAMP)) {
            waterOverrideVaraints[variant.variantID] = true;
        }
    }
    
    @Override
    public void setScanPonds(boolean value) {
        this.doScanPonds = value;
    }
    
    protected boolean skipPondScan(LOTRBiome biome, LOTRBiomeVariant variant) {
        return !doScanPonds || waterOverrideBiomes[biome.biomeID] || waterOverrideVaraints[variant.variantID];
    }
    
    /** If no valid biome ID is found, returns {@link IBiomeDetector#NOT_FOUND}. */
    @Override
    public int getBiomeID(Chunk chunk) {
        LOTRWorldChunkManager manager = (LOTRWorldChunkManager) chunk.worldObj.getWorldChunkManager();
        int blockX = chunk.xPosition << 4;
        int blockZ = chunk.zPosition << 4;
        LOTRBiome biome = (LOTRBiome) manager.getBiomeGenAt(blockX, blockZ);
        LOTRBiomeVariant variant = manager.getBiomeVariantAt(blockX, blockZ);
        
        int waterOccurences = 0;
        int lavaOccurences = 0;
        
        if(!skipPondScan(biome, variant)) {
            for(int x = 0; x < 16; x++) {
                for(int z = 0; z < 16; z++) {
                    int y = chunk.getHeightValue(x, z);
                    if(y > 0) {
                        Block topBlock = chunk.getBlock(x, y-1, z);
                        Block topBlock2 = chunk.getBlock(x, y, z); // For some reason lava doesn't count in height value
                        
                        if(topBlock == Blocks.water) {
                            waterOccurences += 1;
                        } 
                        else if (topBlock2 == Blocks.lava) {
                            lavaOccurences += 1;
                        }
                    }
                }
            }
        }
        
        // Lake and rivers not in variant list so they have no ID
        if(waterOccurences >= 64 || variant == LOTRBiomeVariant.LAKE || variant == LOTRBiomeVariant.RIVER) { // 1/4 of a chunk
            biome = LOTRBiome.river;
            variant = LOTRBiomeVariant.STANDARD;
        }
        else if(lavaOccurences >= 32) { // 1/8 of a chunk
            return ExtTileIdMap.instance().getPseudoBiomeID(ExtTileIdMap.TILE_LAVA);
        }
        
        return MiddleEarthAtlas.proxy.getPsuedoBiomeVariantID(biome, variant);
    }

}
