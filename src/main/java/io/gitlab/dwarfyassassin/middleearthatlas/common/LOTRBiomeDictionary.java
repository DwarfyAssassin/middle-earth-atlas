package io.gitlab.dwarfyassassin.middleearthatlas.common;

import java.util.ArrayList;
import java.util.List;
import io.gitlab.dwarfyassassin.middleearthatlas.common.reflection.MiddleEarthAtlasReflectionHelper;
import lotr.common.LOTRDimension;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import net.minecraftforge.common.BiomeDictionary.Type;

public class LOTRBiomeDictionary {
    
    private static boolean[] registeredBiomes = new boolean[LOTRDimension.MIDDLE_EARTH.biomeList.length];
    private static List<LOTRBiome>[] typeBiomeList = new ArrayList[Type.values().length];
    
    private static boolean[] registeredVariants = new boolean[MiddleEarthAtlasReflectionHelper.getBiomeVariantList().length];
    private static List<LOTRBiomeVariant>[] typeVariantList = new ArrayList[Type.values().length];
    
    public static void registerTypes() { //TODO add other types
        registerBiomeType(LOTRBiome.deadMarshes, Type.SWAMP);
        registerBiomeType(LOTRBiome.midgewater, Type.SWAMP);
        registerBiomeType(LOTRBiome.nurnMarshes, Type.SWAMP);
        registerBiomeType(LOTRBiome.anduinMouth, Type.SWAMP);
        registerBiomeType(LOTRBiome.entwashMouth, Type.SWAMP);
        registerBiomeType(LOTRBiome.lindonCoast, Type.BEACH);
        registerBiomeType(LOTRBiome.longMarshes, Type.SWAMP);
        registerBiomeType(LOTRBiome.nindalf, Type.SWAMP);
        registerBiomeType(LOTRBiome.swanfleet, Type.SWAMP);
        registerBiomeType(MiddleEarthAtlasReflectionHelper.getLOTRBiome("beach"), Type.BEACH);
        registerBiomeType(LOTRBiome.beachGravel, Type.BEACH);
        registerBiomeType(LOTRBiome.farHaradSwamp, Type.SWAMP);
        registerBiomeType(LOTRBiome.farHaradMangrove, Type.SWAMP);
        registerBiomeType(LOTRBiome.shireMarshes, Type.SWAMP);
        registerBiomeType(LOTRBiome.forodwaithCoast, Type.BEACH);
        registerBiomeType(LOTRBiome.farHaradCoast, Type.BEACH);
        registerBiomeType(LOTRBiome.beachWhite, Type.BEACH);
        
        registerVariantType(LOTRBiomeVariant.SWAMP_LOWLAND, Type.SWAMP);
        registerVariantType(LOTRBiomeVariant.SWAMP_UPLAND, Type.SWAMP);
    }
    
    
    public static LOTRBiome[] getBiomesForType(Type type) {
        if(typeBiomeList[type.ordinal()] != null) {
            return typeBiomeList[type.ordinal()].toArray(new LOTRBiome[0]);
        }

        return new LOTRBiome[0];
    }
    
    public static LOTRBiomeVariant[] getVariantsForType(Type type) {
        if(typeVariantList[type.ordinal()] != null) {
            return typeVariantList[type.ordinal()].toArray(new LOTRBiomeVariant[0]);
        }

        return new LOTRBiomeVariant[0];
    }

    public static boolean registerBiomeType(LOTRBiome biome, Type ... types) {
        types = listSubTags(types);

        if(LOTRDimension.MIDDLE_EARTH.biomeList[biome.biomeID] != null) {
            for(Type type : types) {
                if(typeBiomeList[type.ordinal()] == null) {
                    typeBiomeList[type.ordinal()] = new ArrayList<LOTRBiome>();
                }

                typeBiomeList[type.ordinal()].add(biome);
            }
            
            return true;
        }

        return false;
    }

    public static boolean registerVariantType(LOTRBiomeVariant variant, Type ... types) {
        types = listSubTags(types);

        if(MiddleEarthAtlasReflectionHelper.getBiomeVariantList()[variant.variantID] != null) {
            for(Type type : types) {
                if(typeVariantList[type.ordinal()] == null) {
                    typeVariantList[type.ordinal()] = new ArrayList<LOTRBiomeVariant>();
                }

                typeVariantList[type.ordinal()].add(variant);
            }
            
            return true;
        }

        return false;
    }
    
    public static boolean isBiomeRegistered(LOTRBiome biome) {
        return registeredBiomes[biome.biomeID];
    }
    
    public static boolean isVariantRegistered(LOTRBiomeVariant variant) {
        return registeredVariants[variant.variantID];
    }
    
    private static Type[] listSubTags(Type... types) {
        List<Type> subTags = new ArrayList<Type>();

        for (Type type : types) {
            List<Type> subTypes = MiddleEarthAtlasReflectionHelper.getTypeSubTags(type);
            if (subTypes.size() != 0) subTags.addAll(subTypes);
            else subTags.add(type);
        }
 
        return subTags.toArray(new Type[subTags.size()]);
    }
}
