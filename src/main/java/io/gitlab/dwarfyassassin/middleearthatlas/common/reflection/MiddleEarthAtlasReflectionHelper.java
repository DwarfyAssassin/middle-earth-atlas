package io.gitlab.dwarfyassassin.middleearthatlas.common.reflection;

import java.util.ArrayList;
import java.util.List;
import cpw.mods.fml.relauncher.ReflectionHelper;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.biome.variant.LOTRBiomeVariantList;
import net.minecraftforge.common.BiomeDictionary;

public class MiddleEarthAtlasReflectionHelper {

    // Some biomes have the same names as vanilla ones and the same for as the creative tabs happens
    /**
     * Returns the {@link LOTRBiome} with that given field name. This should only be
     * used for biomes where you can't do LOTRBiome.biomeName.
     * 
     * @param name
     *            Name
     * @return {@link LOTRBiome}
     */
    public static LOTRBiome getLOTRBiome(String name) {
        return ReflectionHelper.getPrivateValue (LOTRBiome.class, null, name);
    }
    
    public static List<LOTRBiomeVariant> getLOTRBiomeVariants(LOTRBiomeVariantList variantList) {
        List<Object> bucketList = ReflectionHelper.getPrivateValue(LOTRBiomeVariantList.class, variantList, "variantList");
        Class<? super Object> bucketClass = (Class<? super Object>) LOTRBiomeVariantList.class.getDeclaredClasses()[0];
        
        List<LOTRBiomeVariant> variants = new ArrayList<LOTRBiomeVariant>();
        for(Object bucket : bucketList) {
            variants.add((LOTRBiomeVariant) ReflectionHelper.getPrivateValue(bucketClass, bucket, "variant"));
        }
        
        return variants;
    }
    
    public static List<BiomeDictionary.Type> getTypeSubTags(BiomeDictionary.Type type) {
        return ReflectionHelper.getPrivateValue(BiomeDictionary.Type.class, type, "subTags");
    }
    
    public static LOTRBiomeVariant[] getBiomeVariantList() {
        return ReflectionHelper.getPrivateValue(LOTRBiomeVariant.class, null, "allVariants");
    }
}
