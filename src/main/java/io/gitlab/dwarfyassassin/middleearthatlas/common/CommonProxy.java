package io.gitlab.dwarfyassassin.middleearthatlas.common;

import java.util.ArrayList;
import java.util.List;
import com.google.common.collect.ImmutableList;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import hunternif.mc.atlas.ext.ExtTileIdMap;
import io.gitlab.dwarfyassassin.middleearthatlas.common.reflection.MiddleEarthAtlasReflectionHelper;
import lotr.common.LOTRDimension;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.variant.LOTRBiomeVariant;

public class CommonProxy {
    private ExtTileIdMap extTileIdMap;
    protected List<String> biomeCombinations;
    
    public void init(FMLInitializationEvent event) {
        extTileIdMap = ExtTileIdMap.instance();
        
        calculateBiomeVariantCombinations();
        
        registerMiddleEarthBiomeTiles();
        registerMiddleEarthCustomTiles();
    }

    public void postInit(FMLPostInitializationEvent event) {
        LOTRBiomeDictionary.registerTypes();
        BiomeDetectorMiddleEarth.scanBiomeTypes();
    }
    
    public ImmutableList<String> getBiomeCombinations() {
        return ImmutableList.copyOf(biomeCombinations);
    }
    
    public int getPsuedoBiomeVariantID(LOTRBiome biome, LOTRBiomeVariant variant) {
        return extTileIdMap.getPseudoBiomeID(getBiomeVariantKey(biome, variant));
    }
    
    public String getBiomeVariantKey(LOTRBiome biome, LOTRBiomeVariant variant) {
        String key = biome.biomeName;
        if(variant != LOTRBiomeVariant.STANDARD && variant != null) key += "_" + variant.variantName;
        
        return key;
    }
    
    private void calculateBiomeVariantCombinations() {
        biomeCombinations = new ArrayList<String>();
        
        for(LOTRBiome biome : LOTRDimension.MIDDLE_EARTH.biomeList) {
            if(biome == null) continue;
            biomeCombinations.add(getBiomeVariantKey(biome, LOTRBiomeVariant.STANDARD));
            for(LOTRBiomeVariant variant : MiddleEarthAtlasReflectionHelper.getLOTRBiomeVariants(biome.getBiomeVariantsLarge())) {
                biomeCombinations.add(getBiomeVariantKey(biome, variant));
            }
            for(LOTRBiomeVariant variant : MiddleEarthAtlasReflectionHelper.getLOTRBiomeVariants(biome.getBiomeVariantsSmall())) {
                biomeCombinations.add(getBiomeVariantKey(biome, variant));
            }
        }
        
        MiddleEarthAtlas.logger.info("Found a total of " + biomeCombinations.size() + " biome + biome variant combinations.");
    }
     
    private void registerMiddleEarthBiomeTiles() {
        for(String key : biomeCombinations) {
            extTileIdMap.getOrCreatePseudoBiomeID(key);
        }
        MiddleEarthAtlas.logger.info("Registered a total of " + biomeCombinations.size() + " biome + biome variant combinations.");
    }
    
    private void registerMiddleEarthCustomTiles() {
        
        
    }

}
