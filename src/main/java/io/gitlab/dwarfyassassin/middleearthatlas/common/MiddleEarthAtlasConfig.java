package io.gitlab.dwarfyassassin.middleearthatlas.common;

import java.util.ArrayList;
import java.util.List;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.common.config.Configuration;

public class MiddleEarthAtlasConfig {
    public static boolean factionColoredBiomeTiles;
    public static boolean newTextures = false;
    private static int textureVersion;
    private static boolean prevFactionColoredBiomeTiles;

    private static Configuration config;
    private static List<String> allCategories;
    private static String CATEGORY_CLIENT;
    private static String CATEGORY_INTERNAL;

    public static void setupAndLoad(FMLPreInitializationEvent event) {
        config = new Configuration(event.getSuggestedConfigurationFile());
        load();
    }

    private static void load() {
        factionColoredBiomeTiles = config.get(CATEGORY_CLIENT, "Faction Colored Tiles", true, "If this is set to 'true' biome tiles belonging to a faction will colored in the faction color.").setRequiresMcRestart(true).getBoolean();

        textureVersion = config.get(CATEGORY_INTERNAL, "Texture Version", MiddleEarthAtlas.TEXTURE_VERSION, "Internal texture version value. DO NOT TOUCH.", 1, Integer.MAX_VALUE).setShowInGui(false).setRequiresMcRestart(true).getInt();
        if(textureVersion != MiddleEarthAtlas.TEXTURE_VERSION) {
            newTextures = true;
            config.getCategory(CATEGORY_INTERNAL).get("Texture Version").set(MiddleEarthAtlas.TEXTURE_VERSION);
        }
        prevFactionColoredBiomeTiles = config.get(CATEGORY_INTERNAL, "Previous Faction Colored Tiles", factionColoredBiomeTiles, "Internal previous factionColoredBiomeTiles. DO NOT TOUCH.").setShowInGui(false).setRequiresMcRestart(true).getBoolean();
        if(prevFactionColoredBiomeTiles != factionColoredBiomeTiles) {
            newTextures = true;
            config.getCategory(CATEGORY_INTERNAL).get("Previous Faction Colored Tiles").set(factionColoredBiomeTiles);
        }
        
        config.getCategory(CATEGORY_INTERNAL).setShowInGui(false);
        
        if (config.hasChanged()) config.save();
    }

    private static String newCategory(String category) {
        allCategories.add(category);
        return category;
    }
    
    static {
        allCategories = new ArrayList<String> ();
        CATEGORY_CLIENT = newCategory("client");
        CATEGORY_INTERNAL = newCategory("internal");
    }
}
