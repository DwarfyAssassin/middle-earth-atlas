package io.gitlab.dwarfyassassin.middleearthatlas.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import hunternif.mc.atlas.AntiqueAtlasMod;
import io.gitlab.dwarfyassassin.middleearthatlas.common.command.CommandGetBiomeID;
import lotr.common.LOTRDimension;

@Mod(modid = MiddleEarthAtlas.MODID, name = MiddleEarthAtlas.NAME, version = MiddleEarthAtlas.VERSION, dependencies="required-after:lotr;required-after:antiqueatlas")
public class MiddleEarthAtlas {
    public static final String MODID = "middleearthatlas";
    public static final String NAME = "Middle Earth Atlas";
    public static final String VERSION = "0.1.3";
    public static final int TEXTURE_VERSION = 6;

    public static Logger logger = LogManager.getLogger("MiddleEarthAtlas");

    @SidedProxy(clientSide="io.gitlab.dwarfyassassin.middleearthatlas.client.ClientProxy", serverSide="io.gitlab.dwarfyassassin.middleearthatlas.common.CommonProxy")
    public static CommonProxy proxy;
    

    private final BiomeDetectorMiddleEarth biomeDetectorMiddleEarth = new BiomeDetectorMiddleEarth();

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        MiddleEarthAtlasConfig.setupAndLoad(event);
        
        AntiqueAtlasMod.itemAtlas.setBiomeDetectorForDimension(LOTRDimension.MIDDLE_EARTH.dimensionID, biomeDetectorMiddleEarth);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
        
        AntiqueAtlasMod.itemAtlas.setBiomeDetectorForDimension(LOTRDimension.MIDDLE_EARTH.dimensionID, biomeDetectorMiddleEarth);
        biomeDetectorMiddleEarth.setScanPonds(AntiqueAtlasMod.settings.doScanPonds);
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
        
        
        
        // server
        // TODO villaga en gobal tile
    }
    
    @EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandGetBiomeID());
    }
}
